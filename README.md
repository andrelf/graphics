# My Graphics Repository

Content: Some nice graphics I have made.

Made with: Usually Gimp, Inkscape or XFig.

License: Public Domain. Take it, use it, share it, make whatever you want and don't ask me.
